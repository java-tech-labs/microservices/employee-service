package com.microservices.employee.service;

import com.microservices.employee.payload.EmployeeDTO;

public interface EmployeeService {

    EmployeeDTO getEmployeeById(Integer id);
}
