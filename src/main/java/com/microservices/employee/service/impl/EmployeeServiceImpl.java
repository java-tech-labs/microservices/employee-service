package com.microservices.employee.service.impl;

import com.microservices.employee.entity.Employee;
import com.microservices.employee.exception.EmployeeNotFoundException;
import com.microservices.employee.mapper.EmployeeMapper;
import com.microservices.employee.payload.AddressDTO;
import com.microservices.employee.payload.EmployeeDTO;
import com.microservices.employee.repository.EmployeeRepository;
import com.microservices.employee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final EmployeeMapper employeeMapper;
    private final RestTemplate restTemplate;
    private final WebClient webClient;
    private final DiscoveryClient discoveryClient;
    private final LoadBalancerClient loadBalancerClient;

    @Value("${address-service.base.url}")
    private String addressBaseUrl;


    public EmployeeServiceImpl(EmployeeRepository employeeRepository,
                               EmployeeMapper employeeMapper,
                               RestTemplate restTemplate,
                               WebClient webClient,
                               DiscoveryClient discoveryClient,
                               LoadBalancerClient loadBalancerClient) {
        this.employeeRepository = employeeRepository;
        this.employeeMapper = employeeMapper;
        this.restTemplate = restTemplate;
        this.webClient = webClient;
        this.discoveryClient = discoveryClient;
        this.loadBalancerClient = loadBalancerClient;
    }

    @Override
    public EmployeeDTO getEmployeeById(Integer id) {
        Employee employee = employeeRepository.findById(id)
                .orElseThrow(() -> new EmployeeNotFoundException("Employee with ID " + id + " not found"));
        EmployeeDTO employeeDTO = employeeMapper.mapToDTO(employee);
        AddressDTO addressDTO = callingAddressServiceUsingRestTemplate(id);
        employeeDTO.setAddressDTO(addressDTO);
        return employeeDTO;
    }

//    private AddressDTO callingAddressServiceUsingRestTemplate(Integer id) {
//        return restTemplate.getForObject(addressBaseUrl + "/address/{id}", AddressDTO.class, id);
//    }

//    private AddressDTO callingAddressServiceUsingRestTemplate(Integer id) {
//        List<ServiceInstance> instancesOfAddressService = discoveryClient.getInstances("address-service");
//        ServiceInstance serviceInstance = instancesOfAddressService.get(0);
//        String uri = serviceInstance.getUri().toString();
//        return restTemplate.getForObject(uri + "/app-address/api/address/{id}", AddressDTO.class, id);
//    }

    private AddressDTO callingAddressServiceUsingRestTemplate(Integer id) {
        ServiceInstance serviceInstance = loadBalancerClient.choose("address-service");
        String uri = serviceInstance.getUri().toString();
        String contextPath = serviceInstance.getMetadata().get("contextPath");
        System.out.println("uri = " + uri + contextPath);
        return restTemplate.getForObject(uri + contextPath + "/address/{id}", AddressDTO.class, id);
    }


    private AddressDTO callingAddressServiceUsingWebClient(Integer id) {
        return webClient
                .get()
                .uri("/address/" + id)
                .retrieve()
                .bodyToMono(AddressDTO.class)
                .block();
    }

}
