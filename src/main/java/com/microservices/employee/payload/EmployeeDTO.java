package com.microservices.employee.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeDTO {

    private Integer id;
    private String name;
    private String email;
    private String bloodGroup;
    private AddressDTO addressDTO;
}
